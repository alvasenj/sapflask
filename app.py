from flask import Flask, render_template, request, url_for
from flask_migrate import Migrate
from werkzeug.utils import redirect

import parametros
from database import db
from forms import PersonaForm
from models import Persona

app = Flask(__name__)

# Configuracion de la bd
USER_DB = parametros.user
PASS_DB = parametros.password
URL_DB = parametros.host
NAME_DB = parametros.database
FULL_URL_DB = f'postgresql://{USER_DB}:{PASS_DB}@{URL_DB}/{NAME_DB}'

app.config['SQLALCHEMY_DATABASE_URI'] = FULL_URL_DB
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Inicializacion del objeto db de sqlalchemy
db.init_app(app)

# Configuracion de flask-migrate
migrate = Migrate()
migrate.init_app(app, db)

# configuracion de flask-wtf
app.config['SECRET_KEY'] = 'llave_secreta'


@app.route('/')
def inicio():
    # Listado de personas
    personas = Persona.query.order_by('id')
    no_personas = Persona.query.count()
    app.logger.debug(f'Listado Personas: {personas}')
    app.logger.debug(f'Total Personas: {no_personas}')
    return render_template('index.html', personas_key=personas, total_personas=no_personas)


@app.route('/ver/<id>')
def ver_persona(id):
    # Recuperamos la persona segun su id proporcionado
    # persona = Persona.query.get(id)
    persona = Persona.query.get_or_404(id)
    app.logger.debug(f'Ver persona: {persona}')
    return render_template('detalle.html', persona=persona)


@app.route('/agregar', methods=['GET', 'POST'])
def agregar():
    persona = Persona()
    persona_form = PersonaForm(obj=persona)
    if request.method == 'POST':
        if persona_form.validate_on_submit():
            persona_form.populate_obj(persona)
            app.logger.debug(f'Persona a insertar: {persona}')
            #Insertamos nuevo registro
            db.session.add(persona)
            db.session.commit()
            return redirect(url_for('inicio'))
    return render_template('agregar.html', forma=persona_form)


@app.route('/editar/<int:id>', methods=['GET', 'POST'])
def editar_persona(id):
    persona = Persona.query.get_or_404(id)
    persona_form = PersonaForm(obj=persona)
    if request.method == 'POST':
        if persona_form.validate_on_submit():
            persona_form.populate_obj(persona)
            app.logger.debug(f'Persona a editar: {persona}')
            # Editamos registro. Al estar en la transaccion debido a haberla recuperado a  taves de la query
            # no es necesario ejecutar .add
            db.session.commit()
            return redirect(url_for('inicio'))
    return render_template('editar.html', forma=persona_form)


@app.route('/eliminar/<int:id>')
def eliminar_persona(id):
    persona = Persona.query.get_or_404(id)
    app.logger.debug(f'Persona a eliminar: {persona}')
    db.session.delete(persona)
    db.session.commit()
    return redirect(url_for('inicio'))